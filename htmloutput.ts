import { EventEmitter } from "events";
import * as express from 'express';
import * as Entities from 'html-entities';

import { GroupStats, LogEvent } from "./types";

const app = express();
const entities = new Entities.Html5Entities();

let staticHeader = `<!doctype html>
  <html lang='en'>
  <head>
  <style>
  body {
    margin: 2px;
  }
  .region > *{
    padding-left:2em;
    text-indent: -0.5em
  }
  .toggle {
    float: left;
    margin-top: -1.1em;
    margin-left: -1.5em;
  }
  }</style>
  <title>log</title>
  </head>
  <body>`;
let staticFooter = `
<script lang="javascript">
  for (let v of document.getElementsByClassName('toggle')){
    console.log(v);
  }
</script>
</body></html>`;
let staticContent : string = '';
let level = 0;
let indentLevel = 0;
const bufferedConsole : () => {log: (line: string, encode?: boolean)=> void, flush: () => void, addRegion: () => void, closeRegion: () => void} = () => {
  let buffer: string = undefined;
  let count = 1;
  const regionStart = "<span class='toggle'>[*]</span><div class=\"region\">";
  const flush = () => {
    if (!buffer || !count) return;
    while (level < indentLevel){
      staticContent += "</div>";
      --indentLevel;
    }
    if (count === 1) staticContent += `<div class="row">${buffer}</div>`;
    else staticContent += `<div class="row">${buffer}  <span class="repetition">REPEATED ${count} TIMES</span></div>`;
    while (level > indentLevel){
      staticContent += regionStart;
      ++indentLevel;
    }
    count = 1;
    buffer = undefined;
  };

  const addRegion = () => {
    level ++;
  };
  const closeRegion = () => {
    level --;
  };
  return {
    log: (line:string, encode:boolean = true) => {
      const l2 = (encode ? entities.encode(line) : line);
      if (buffer === l2) count ++;
      else {
        flush();
        buffer = l2;
      }
    },
    flush,
    addRegion,
    closeRegion
  }
};

const bc = bufferedConsole();

const HtmlOutput : (e:EventEmitter) => void = (e:EventEmitter) => {
  e.on("start", (fn:string)=>{bc.log(`Parsing file ${fn}`)});  
  e.on("end", (fn:string)=>{bc.log(`Done parsing file ${fn}`); bc.flush(); console.log('parsed');});
  e.on("groupEnd", (gs: GroupStats)=>{
    bc.log('<div class="group">', false);
    bc.log(`[GROUP] ${gs.ev} --- ${gs.cnt} times`);
    bc.log('</div>', false);
  });
  e.on("regionEnd", () => {bc.closeRegion()});
  e.on("regionStart", () => {bc.addRegion(); bc.flush();});
  e.on("row", (row:LogEvent)=>{
      bc.log(`[${row.soqlcount}] ${row.event} --- ${row.src} - @${Math.floor((row.time * 1000)) / 1000}s`);
  })

};
app.get('/', (_, res, next) => {
  res.send(`${staticHeader}${staticContent}${staticFooter}`);
  next();
});
app.listen(3000);
console.log('Listening on port 3000');
export default HtmlOutput;
