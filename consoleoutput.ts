import { EventEmitter } from "events";

import { GroupStats, LogEvent } from "./types";
let padding = '';
const addPad = () => padding += '  ';
const removePad = () => padding = padding.substr(2);

const pad = (SOQLCount: number) => `[${ ('  ' + SOQLCount).substr(-3)}]${padding}`;

const bufferedConsole : () => {log: (line: string)=> void, flush: () => void} = () => {
  let buffer: string = undefined;
  let count = 1;
  const flush = () => {
    if (!buffer || !count) return;
    if (count === 1) console.log(buffer);
    else console.log(`${buffer}  <<<< REPEATED ${count} TIMES`);
    count = 1;
    buffer = undefined;
  };
  return {
    log: (line:string) => {
      if (buffer === line) count ++;
      else {
        flush();
        buffer = line;
      }
    },
    flush
  }
};
const bc = bufferedConsole();

const ConsoleOutput : (e : EventEmitter) => void =(e) => {
  e.on("start", (fn:string)=>{bc.log(`Parsing file ${fn}`)});  
  e.on("end", (fn:string)=>{bc.log(`Done parsing file ${fn}`); bc.flush()});
  e.on("groupEnd", (gs: GroupStats)=>{
    bc.log(`${pad(gs.soql)}[GROUP] ${gs.ev} --- ${gs.cnt} times`);
  });
  e.on("regionEnd", removePad);
  e.on("regionStart", addPad);
  e.on("row", (row:LogEvent)=>{
    bc.log(`${pad(row.soqlcount)}${row.event} --- ${row.src} - @ ${Math.floor(row.time * 1000) / 1000}`);
  })
};

export default ConsoleOutput;
