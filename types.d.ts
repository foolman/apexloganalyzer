export type GroupDef = {
  [index:string] : {
    event?: string;
    prefix?: string;
    namefn?: (row: LogEvent) => string;
  }
};

export type LogEvent = {
  event: string,
  src: string,
  soqlcount: number,
  time: number
};

export type Qualifier = {
  [index:string] : boolean
};

export type EventConfig = {
  ignored: Qualifier,
  regionStart : Qualifier,
  regionEnd : Qualifier,
  groups : GroupDef
};

export type GroupStats = {
  ev : string,
  cnt : number,
  soql : number,
};

export interface IOutput {
  (events: Array<LogEvent>) : void;
}
