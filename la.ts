import { EventEmitter } from "events";
import { existsSync, readFileSync } from "fs";

import { GroupStats, LogEvent, EventConfig } from "./types";
import ConsoleOutput from './consoleoutput';
import HtmlOutput from './htmloutput';

const fn = process.argv[2];
if (!fn) {
  console.log('Please specify an existing file');
  process.exit(-1);
}

const defaultConfig : EventConfig = {
  ignored :  {
    LIMIT_USAGE_FOR_NS: true,
    HEAP_ALLOCATE: true,
    STATEMENT_EXECUTE: true,
  },
  regionStart : {
    CONSTRUCTOR_ENTRY : true,
    METHOD_ENTRY: true,
    SYSTEM_METHOD_ENTRY: true,
    CODE_UNIT_STARTED: true
  },
  regionEnd : {
    CONSTRUCTOR_EXIT : true,
    METHOD_EXIT: true,
    SYSTEM_METHOD_EXIT: true,
    CODE_UNIT_FINISHED: true
  },
  groups: {
    FETCHRESULT : {
      prefix : "Database.QueryLocatorIterator"
    },
    ENTERING_MANAGED_PKG :{
      event : "ENTERING_MANAGED_PKG",
      namefn : ({event, src} : LogEvent) => `${event} - ${src}`
    }
  }
};

const parseFile : (filename : string) => Array<LogEvent> = (fn) => {
  if (!existsSync(fn)) throw new Error(`File ${fn} does not exist`);

  const createParser : () => (l:string) => LogEvent = () => {
    let soqlcount = 0;
    return (l: string) : LogEvent => {
      const parts = l.split('|');
      const timeMatches = /.*\((\d+)\)/.exec(parts[0]);
      let time = 0;
      if (timeMatches) {
        time = Number.parseInt( timeMatches[1] ) / (10**9);
      }
      if (parts[1] === 'SOQL_EXECUTE_BEGIN') soqlcount++;
      return {event: parts[1],src: parts.pop(), soqlcount, time};
   }
  };

  return readFileSync(fn).toString().split('\n').map(createParser())
};

class Output extends EventEmitter {
  config : EventConfig;

  protected isRegionStart({event} : LogEvent){
    return !!this.config.regionStart[event];
  } 
  protected isRegionEnd({event} : LogEvent){
    return !!this.config.regionEnd[event];
  }

  protected isIgnored({event} : LogEvent){
    return !event || !!this.config.ignored[event];
  }

  protected groupName(row : LogEvent) : string {
    for (let k in this.config.groups){      
      const def = this.config.groups[k];
      if (def.event && row.event === k){
        return (def.namefn ? def.namefn(row) : k);
      }else if (def.prefix && row.src.startsWith(def.prefix)){
        return (def.namefn ? def.namefn(row) : k);
      }
    }
    return undefined;
  }
  
  parse(fn:string){
    this.generate(fn, parseFile(fn));
  }

  generate(label: string, f:Array<LogEvent>) {
    this.emit('start', label);
    let count : GroupStats = {
      ev: undefined,
      cnt: 0,
      soql: 0
    };
    let lastRegion : string = undefined;
    let lastRow : LogEvent;
    for (let row of f) { // iterate events
      if (this.isIgnored(row)) 
        continue;
      const gn = this.groupName(row);
      count.soql = row.soqlcount;
      if (gn) {
        if (count.ev !== gn) {
          if (count.cnt) this.emit("groupEnd",count);
          count.ev = gn;
          count.cnt = 0;
        }
        count.cnt++;
        continue;
      } else if (count.cnt) {
        this.emit("groupEnd",count);
        count.cnt = 0;
        count.ev = undefined;
      }

      if (this.isRegionEnd(row)){
        if (lastRegion === row.src){
          row.event = 'METHOD';
          this.emit('row', row);
        } else if (`${row.src}.${row.src}()` === lastRegion){
          row.event = 'CONSTRUCTOR';
          this.emit('row', row);
        } else{
          this.emit('regionEnd');
          this.emit('row', row);
        }
        lastRow = undefined;
        lastRegion = undefined;
      } else if (this.isRegionStart(row)) {
        if (lastRegion){
          this.emit('row', lastRow);
          this.emit('regionStart');
        }
        lastRegion = row.src;
        lastRow = row;
      }else {
        if (lastRow) {
          this.emit('row', lastRow);
          this.emit('regionStart');
          lastRow = undefined;
          lastRegion = undefined;
        }
        this.emit('row', row);
      }
    }
    this.emit('end', label);
  }
}

const cout = new Output();

cout.config =defaultConfig;
ConsoleOutput(cout);
HtmlOutput(cout);
cout.parse(fn);
